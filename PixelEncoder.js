(function(window) {
    VERSION = "0.1"; // always 3 characters
    CHARACTERS = " .ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!\"£$%^&*()_+-=[]{};'#:@~,/<>?\nàáâãäåæçèéêëìíîïñòóôõöœùúûűüýÿß¢";
    // 127 characters here, currently doubled in code for 254

    function PixelEncoder(canvas) {
        this._canvas = canvas;
        this._ctx = canvas.getContext("2d");
    }

    PixelEncoder.prototype._dimension = 0;
    PixelEncoder.prototype.CHARACTERSLEN = CHARACTERS.length - 1;

    /**
     * Encoder
     */
    
    // Encodes text and outputs the result to the previously set canvas
    PixelEncoder.prototype.encode = function(text) {
        text = text.replace(/[\u2018\u2019]/g, "'")
                   .replace(/[\u201C\u201D]/g, '"')
                   .replace(/\u2014/g, "-"); // convert smart quotes
        text = this.removeUnsupported(text);
        text = VERSION + text;

        var totalPixels = text.length / 3; // RGB
        this._dimension = Math.ceil(Math.sqrt(totalPixels));

        this._canvas.setAttribute("width", this._dimension + "px");
        this._canvas.setAttribute("height", this._dimension + "px");
        this._ctx.clearRect(0,0,this._dimension,this._dimension);

        var pixelArray = this.convertStringToPixels(text);

        for (var y = 0; y < this._dimension; ++y) {
            for (var x = 0; x < this._dimension; ++x) {
                var px = pixelArray[y * this._dimension + x];
                if (!px) continue;
                this._ctx.fillStyle = "rgba(" + px.r + "," + px.g + "," + px.b + ",1)";
                this._ctx.fillRect(x, y, 1, 1);
            }
        }
    };

    // Utlity
    PixelEncoder.prototype.removeUnsupported = function(str) {
        for (var i = 0; i < str.length; ++i) {
            var character = str[i];
            var index = CHARACTERS.indexOf(character);

            if (index == -1) {
                str = str.substr(0,i) + str.substr(i+1);
            }
        }

        return str;
    };

    PixelEncoder.prototype.convertStringToPixels = function(str) {
        var pixelArray = [];

        var i = 0;
        while (i < str.length) {
            var r = -1, g = -1, b = -1;

                r = this.characterToPixel(str[i]);
                i++;

                g = this.characterToPixel(str[i]);
                i++;

                b = this.characterToPixel(str[i]);
                i++;

            pixelArray.push(new Pixel(r*2,g*2,b*2));
        }

        return pixelArray;
    };

    // Unknown characters shouldn't be added at all, the -1 will be ignored
    PixelEncoder.prototype.characterToPixel = function(character) {
        return CHARACTERS.indexOf(character);
    }

    /**
     * Decoder
     */
    function PixelDecoder() {}
    PixelDecoder.prototype.events = {};
    
    PixelDecoder.prototype.decode = function(data) {
        var c = document.querySelector(".decode canvas");
        var ctx = c.getContext("2d");
        var img = new Image();
        img.decoder = this;
        img.onload = function() {
            c.setAttribute("width", img.width);
            c.setAttribute("height", img.height);
            ctx.clearRect(0,0,img.width,img.height);
            ctx.drawImage(img, 0, 0);
            var imageData = ctx.getImageData(0,0, img.width, img.height).data;
            this.decoder.parseImageData(imageData);
        };
        img.src = data;
    };

    PixelDecoder.prototype.parseImageData = function(data) {
        var str = "";

        data = data.slice(4); // remove first pixel containing version

        for (var i = 0; i < data.length; ++i) {
            if (data[i] == 255) continue; // ignore alpha components
            str += this.pixelToCharacter(data[i]/2);
        }

        this.fireEvent("ondecode", str);
    };

    PixelDecoder.prototype.addEventListener = function(type, fn) {
        if (!this.events[type]) this.events[type] = [];
        this.events[type].push(fn);
    };

    PixelDecoder.prototype.fireEvent = function(type, data) {
        if (!this.events[type]) return;

        var events = this.events[type];
        for (var i = 0; i < events.length; ++i) {
            events[i](data);
        }
    };

    PixelDecoder.prototype.removeEventListener = function(type, fn) { 
        if (!this.events[type]) return;

        var events = this.events[type];
        for (var i = 0; i < events.length; ++i) {
            if (events[i] == fn) {
                events.splice(i,1);
            }
        }
    };

    PixelDecoder.prototype.pixelToCharacter = function(pixel) {
        return CHARACTERS[pixel];
    }

    // Pixel Class
    function Pixel(r,g,b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    window.PixelEncoder = PixelEncoder;
    window.PixelDecoder = PixelDecoder;
})(window);