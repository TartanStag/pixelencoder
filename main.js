var encoder, decoder;

init();

function init() {
    encoder = new PixelEncoder(document.querySelector(".encode canvas"));
    decoder = new PixelDecoder();
    decoder.addEventListener("ondecode", onDecoderDecode);

    document.querySelector(".encode .convert").onclick = encodeButtonOnClick;
    document.querySelector(".decode .convert").onclick = decodeButtonOnClick;
    document.querySelector(".decode .image-loader").onchange = decodeImageOnChange;
}

function decodeButtonOnClick() {
    decoder.decode(document.querySelector(".decode input").value);
}

function onDecoderDecode(evt) {
    console.log("ondecode", evt);
    document.querySelector(".decode textarea").value = evt.trim();
}

function decodeImageOnChange() {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (evt) {
            decoder.decode(evt.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
}

function encodeButtonOnClick() {
    var textarea = document.querySelector(".encode textarea");
    encoder.encode(textarea.value);

    var zoom = 20;
    var zoomctx = document.querySelector(".encode .zoom").getContext('2d');
    zoomctx.canvas.setAttribute("width", encoder._dimension * zoom + "px");
    zoomctx.canvas.setAttribute("height", encoder._dimension * zoom + "px");
    zoomctx.imageSmoothingEnabled = false;
    zoomctx.clearRect(0,0,encoder._dimension * zoom,encoder._dimension * zoom);
    zoomctx.drawImage(encoder._canvas, 0,0,
                    encoder._dimension, encoder._dimension,
                    0,0,
                    encoder._dimension * zoom, encoder._dimension * zoom);
    document.querySelector(".generate-output").value = encoder._canvas.toDataURL();
}